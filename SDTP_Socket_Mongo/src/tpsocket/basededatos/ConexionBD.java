package tpsocket.basededatos;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import java.net.UnknownHostException;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
/**
 *
 * @author Osmar
 */
public class ConexionBD {
    private String host;
    private Integer puerto;
    private String bd;
    private MongoClient mongo;
    private DB basededatos;
    
    public ConexionBD(String host, Integer puerto, String bd) {
        this.host = host;
        this.puerto = puerto;
        this.bd = bd;
        this.mongo=null;
        this.basededatos=null;
    }

    public ConexionBD() {
        this.host = null;
        this.puerto = null;
        this.bd = null;
        this.mongo=null;
        this.basededatos=null;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPuerto() {
        return puerto;
    }

    public void setPuerto(Integer puerto) {
        this.puerto = puerto;
    }

    public String getBd() {
        return bd;
    }

    public void setBd(String bd) {
        this.bd = bd;
    }
    
    public void crear() throws UnknownHostException
    {
        this.mongo= new MongoClient(this.host,this.puerto);   //crea el objeto Mongo Cliente
        this.basededatos=this.mongo.getDB(bd);  //Consigue el objeto DB (base de datos)

    }
    
    
    public void insertar(String coleccion, String StringJson) throws Throwable  //insertar en bases de datos
    {
          DBCollection collection;
          collection = basededatos.getCollection(coleccion);   //consigue la coleccion-tabla
          
          DBObject dbObject = (DBObject) JSON.parse(StringJson); //convierte un StringJson a un objeto de la base de datos
          collection.insert(dbObject);                          //inserta el objeto a la coleccion especificada
    }
}

