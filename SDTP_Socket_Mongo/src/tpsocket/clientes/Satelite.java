package tpsocket.clientes;

import tpsocket.constantes.Defaults;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import tpsocket.imgmanager.ImgEncoderB64;
import tpsocket.logs.LogFileManager;
import tpsocket.tcp.TCPComManager;
import com.google.gson.Gson;
import java.awt.HeadlessException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Random;

/** Representa a la entidad Satelite de la especificacion.
 *
 * @author hean
 */
public class Satelite {
    /** Variables definidas en la especificacion. */
    private String  depto, distto, img_Procesado_a_Base64, typeOfImg;
    private int identificador;
    private float posX, posY, codCultivo;
    private boolean cargaDeDatosFinalizada;
    
    /** Constructor.
     * @param nombre del satelite.
     */
    public Satelite(int id)
    {
        this.identificador = id;
    }
    
    /** Tarea de constructor independizado para mejor comprension.
     * @throws IOException si se produce error al abrir archivo o img.
     */
    public void capturarImagen() throws IOException
    {
        this.cargaDeDatosFinalizada = false;
        JFileChooser fileChooser = new JFileChooser();
        /* Abre una ventana de selección de archivos.
        * El método .showOpenDialog(objeto usado para centrar la ventanita)
        * devuelve un enterito que indica la opción que se seleccionó: Aceptar, Cancelar.
        */
        if(fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
        {
            // Obtiene un puntero al archivo seleccionado.
            File archivoSelecto = fileChooser.getSelectedFile();
            // Extrae la ruta completa del archivo seleccionado.
            StringBuilder imagenRutaAbsoluta = new StringBuilder(archivoSelecto.getAbsolutePath());
            // Comprobar si es un archivo de imagen.
            if(!this.isImgFile(imagenRutaAbsoluta.toString()))
            {
                JOptionPane.showMessageDialog(null, "No es una img.\n"
                        + "No se creara ningun satelite.", "ERROR", JOptionPane.ERROR_MESSAGE);
            }
            else
            {
                /** Crear un buffer de Imagen (se necesita en este formato para poder aplicarle el BASE 64). */
                BufferedImage imageBuffer = ImageIO.read(new File(imagenRutaAbsoluta.toString()));

                /** Obtener longitud de la ruta completa. */
                int longitudPath = imagenRutaAbsoluta.length();
                /** Extraer la extension del archivo. */
                this.typeOfImg = imagenRutaAbsoluta.delete(0, longitudPath-3).toString();

                /** Codificar la imagen, pasandole el buffer de imagen y el tipo de imagen. */
                /** Guarda en el atributo. */
                this.img_Procesado_a_Base64 = ImgEncoderB64.encodeToString(imageBuffer, this.typeOfImg);
//                this.img_Procesado_a_Base64 = "lalalala";

                /** Crea un lector de buffer, desde la entrada estandard: teclado. */
                /** Es para introducir los datos asociados a la imagen a travéz
                    de la consola. */
//                BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));

//                JOptionPane.showMessageDialog(null, "Ahora completa los datos que te pide."
//                        + "\nFijate en la terminal (Ubuntu) o en la salida (netbeans).", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                this.depto = Defaults.DataSatelite[0][0].toString();
                this.distto = Defaults.DataSatelite[1][(int) (Math.random() * 4)].toString();
                this.posX = Float.parseFloat(Defaults.DataSatelite[2][(int) (Math.random() * 4)].toString());
                this.posY = Float.parseFloat(Defaults.DataSatelite[3][(int) (Math.random() * 4)].toString());
                this.codCultivo = (float) Math.random();
//                System.out.print("Departamento: ");
//                this.depto = teclado.readLine();
//                System.out.print("Distrito o Ciudad: ");
//                this.distto = teclado.readLine();
//                System.out.print("Coordenada X: ");
//                this.posX = Float.parseFloat(teclado.readLine());
//                System.out.print("Coordenada Y: ");
//                this.posY = Float.parseFloat(teclado.readLine());

                /** Bandera de control. */
                this.cargaDeDatosFinalizada = true;
                
                /** Seteado del objeto Satelite Finalizada. */
            }
        }
    }
    
    /** Analiza si es una imagen u otro tipo de archivo.
     * 
     * @param fullPath Ruta completa del archivo a analizar.
     * @return true si es una imagen: png, jpg, bmp u otros (agregar si hay otro tipo de imagen)
     */
    private boolean isImgFile(String fullPath)
    {
        /** Obtener la longitud de la ruta completa. */
        int longitud = fullPath.length();
        /** Extraer los ultimos 4 caracteres y pasa a minuscula. */
        String substring = fullPath.substring(longitud-4, longitud).toLowerCase();
        /** Retorna true si es uno de los tipos especificados. */
        return substring.contains(".png") || 
                substring.contains(".jpg") || substring.contains(".bmp");
    }

    public String getDepto() {
        return depto;
    }

    public void setDepto(String depto) {
        this.depto = depto;
    }

    public String getDistto() {
        return distto;
    }

    public void setDistto(String distto) {
        this.distto = distto;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public String getTypeOfImg() {
        return typeOfImg;
    }

    public float getCodCultivo() {
        return codCultivo;
    }

    public void setCodCultivo(float codCultivo) {
        this.codCultivo = codCultivo;
    }

    public String getImgProcesadoABase64() {
        return img_Procesado_a_Base64;
    }

    public void setImagenJS(String imagenJS) {
        this.img_Procesado_a_Base64 = imagenJS;
    }
    
    /** Verifica si se efectuo la apertura de imagen correctamente.
     * @return true si logro abrir imagen, false si no logro.
     */
    public boolean finishedFillingData() {
        return this.cargaDeDatosFinalizada;
    }
    
    public void escribirLog(String msg)
    {
        try
        {
            System.out.println("[Sat-"+this.getIdentificador()+"]: "+msg);
            LogFileManager log = new LogFileManager(Defaults.SATELITE_LOG_FILEPATH, true);
            log.writeTo("[Sat-"+this.getIdentificador()+"]: "+msg);
            log.closeWriter();
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "IOException desde escribirLog() de "+this.getClass().getName(), "Exception", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }
    
    public static void main(String argv[])
    {
        StringBuilder host;
        try
        {
            System.out.print("IP del Servidor: ");
            BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
            host = new StringBuilder(teclado.readLine());

            /** Conexion. */
            Socket clienteSocket;
            clienteSocket = new Socket(InetAddress.getByName(host.toString()), Defaults._SERVER_OF_SATELITE_TCP_PORT);
            //clienteSocket = new Socket(InetAddress.getByName("192.168.2.11"), Defaults._SERVER_OF_SATELITE_TCP_PORT);

            /** Crear un objeto que maneje los envios. */
            TCPComManager mensajeManager = new TCPComManager();

            do {
                /** Instancia un objeto Satelite. */
                Satelite satelite = new Satelite(clienteSocket.getLocalPort());
                /** Método que simula la captura de img.
                 * Dentro del método, abre un cuadro de diálogo que pide seleccionar
                 * un archivo.
                 * Verifica que sea de un tipo de archivo imagen (png, jpg, bmp)
                 * y codifica esa imagen en B64. Guarda dentro de un atributo
                 * la imagen codificada como String.
                 */
                satelite.capturarImagen();

                /** Comprueba si se asignó los valores a todos sus atributos. */
                if(satelite.finishedFillingData())
                {
                    StringBuilder log;
                    log = new StringBuilder("Objeto satelite instaciando y establecido sus valores correctamente.");
                    satelite.escribirLog(log.toString());
                    log = log.delete(0, log.length());

                    /** Crea un objeto JSON. */
                    Gson objGson = new Gson();

                    /** Gson-near el objeto Satelite. */
                    String objetoSateliteEnGson = objGson.toJson(satelite);
                    log.append("Objeto satelite formateado a Gson.");
                    satelite.escribirLog(log.toString());
                    log = log.delete(0, log.length());

                    /** Obtener la longitud del mensaje en bytes. */
                    int longitud = objetoSateliteEnGson.getBytes().length;

                    /** Negociacion - Enviar la longitud al servidor. */
                    mensajeManager.sendInt(longitud, clienteSocket);

                    /** Si el servidor recibio bien la longitud. */
                    if(mensajeManager.readString(clienteSocket).equals("ok"))
                    {
                        log.append("Longitud del msg gson enviada correctamente."
                                + " Enviando msg gson al servidor...");
                        satelite.escribirLog(log.toString());
                        log = log.delete(0, log.length());
                        /** Envia el mensaje en flujo de bytes. */
                        mensajeManager.sendBytes(objetoSateliteEnGson, clienteSocket);
                    }

                    log.append("Buffer de salida Llenado. Esperando respuesta del servidor...");
                    satelite.escribirLog(log.toString());
                    log = log.delete(0, log.length());
                    /** Si el servidor recibio bien el mensaje. */
                    if(mensajeManager.readString(clienteSocket).equals("ok"))
                    {
                        log.append("Servidor ha respondido. Mensaje enviado correctamente.");
                        satelite.escribirLog(log.toString());
                        log = log.delete(0, log.length());

                        StringBuilder msgTemp = new StringBuilder(objetoSateliteEnGson);

                        log.append("Mensaje enviado: "+msgTemp.substring(0, 130)+"... y mas ...");
                        satelite.escribirLog(log.toString());
                        JOptionPane.showMessageDialog(null, "Mensaje enviado correctamente.", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                        log = log.delete(0, log.length());
                    }
                }
                else /** si no se pudo instanciar bien al satelite. */
                {
                    JOptionPane.showMessageDialog(null, "No se completo la carga de datos."
                            + "\nNo se enviara nada al servidor.", "Error", JOptionPane.ERROR_MESSAGE);
                }
                
                System.out.println();
            }
            while(JOptionPane.showConfirmDialog(null,
                "¿Caputrar otra imagen?", "Mensaje",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION);

            /** Avisa al servidor de su desconexion. */
            mensajeManager.sendInt(0, clienteSocket);
            /** Cierra la conexion cerrando el socket. */
            clienteSocket.close();
        }
        catch(IOException | HeadlessException oe)
        {
            JOptionPane.showMessageDialog(null, "Exception desde main() de Satelite."
                    + "\nRevisar la consola.", "Exception", JOptionPane.ERROR_MESSAGE);
            oe.printStackTrace();
        }
    }
}
