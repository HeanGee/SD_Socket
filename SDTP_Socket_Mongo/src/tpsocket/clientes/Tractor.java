
package tpsocket.clientes;
import tpsocket.constantes.Defaults;
import tpsocket.logs.LogFileManager;
import tpsocket.tcp.TCPComManager;
import com.google.gson.Gson;
import java.awt.HeadlessException;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Random;
import javax.swing.JOptionPane;
/**
 *
 * @author Fernando
 */
public class Tractor {
    
    private int identificador;
    private Integer altura;
    private Integer  peso;
    private Integer temperatura;
    private Integer humedad;
    
    /** Constructor.
     * @param id
     */
    public Tractor(int id){
        this.identificador=id;
        this.altura=this.peso=this.temperatura=0;
    }
    /*Getters de los atributos*/
    public Integer getIdentificador(){
        return this.identificador;
    }
    public Integer getAltura(){
        return this.altura;
    }
    public Integer getPeso(){
        return this.peso;
    }
    public Integer getTemperatura(){
        return this.temperatura;
    }
    public Integer getHumedad(){
        return this.humedad;
    }
    /*Metodo encargado de leer los datos de los sensores.*/
    public void captarDatos()
    {
        Random r= new Random();
        // TODO
        // Puede venir aquí la operación de completar los atributos.
        System.out.println("Los sensores del tractor: "+this.getIdentificador()+" estan captando los datos necesarios...");
        
        this.altura=1+r.nextInt(10);
        System.out.println("El valor optenido para la altura: "+this.altura+" m");
        this.peso=5+r.nextInt(20);
        System.out.println("El valor optenido para la peso: "+this.peso+" Tn");
        this.temperatura=r.nextInt(40);
        System.out.println("El valor optenido para la temperatura: "+this.temperatura+" ºC");
        this.humedad=r.nextInt(100);
        System.out.println("El valor optenido para la humedad: "+this.humedad+" %");
    }
    
    public static void main(String argv[])
    {
        try
        {
            StringBuilder host;
            System.out.print("IP del Servidor: ");
            BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
            host = new StringBuilder(teclado.readLine());
            /** Conexion. */
            Socket clienteSocket;
            clienteSocket = new Socket(InetAddress.getByName(host.toString()), Defaults._SERVER_OF_TRACTOR_TCP_PORT);
            DataOutputStream dos = new DataOutputStream(clienteSocket.getOutputStream());
            DataInputStream dis = new DataInputStream(clienteSocket.getInputStream());
            
            /** Crear un objeto que maneje los envios. */
            TCPComManager mensajeManager = new TCPComManager();

            do {
                /** Instancia un objeto Tractor. */
                Tractor tractor = new Tractor(clienteSocket.getLocalPort());
                
                /* Capturamos los datos necesarios.
                 */
                tractor.captarDatos();
                
                StringBuilder log;
                log = new StringBuilder("Objeto tractor instaciando y establecido sus valores correctamente.");
                tractor.escribirLog(log.toString());
                log = log.delete(0, log.length());

                /** Crea un objeto JSON. */
                Gson objGson = new Gson();

                /** Gson-near el objeto Satelite. */
                String objetoTractorEnGson = objGson.toJson(tractor);
                log.append("Objeto tractor formateado a Gson correctamente.");
                tractor.escribirLog(log.toString());
                log = log.delete(0, log.length());

                /** Envia el mensaje en flujo de bytes. */
                log.append("Enviando msg al servidor...");
                tractor.escribirLog(log.toString());
                log = log.delete(0, log.length());
                dos.writeUTF(objetoTractorEnGson);
                
                /** Si el servidor recibio bien el mensaje. */
                log.append("Esperando respuesta del servidor...");
                tractor.escribirLog(log.toString());
                log = log.delete(0, log.length());
                
                if(dis.readUTF().equals("ok")) 
                {
                    log.append("Mensaje al servidor enviado correctamente.");
                    tractor.escribirLog(log.toString());
                    JOptionPane.showMessageDialog(null, log, "Mensaje", JOptionPane.DEFAULT_OPTION);
                }
                log.append("Fin de un bucle");
                tractor.escribirLog(log.toString());
                log = log.delete(0, log.length());
                
                System.out.println();
            }
            while(JOptionPane.showConfirmDialog(null,
                "¿Caputrar mas datos desde el Tractor?", "Mensaje",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION);
            
            /** Avisa al servidor de su desconexion. */
            mensajeManager.sendString("no", clienteSocket);
            /** Cierra la conexion cerrando el socket. */
            clienteSocket.close();
        }
        catch(IOException | HeadlessException oe)
        {
            JOptionPane.showMessageDialog(null, "Exception desde main() de Tractor."
                    + "\nRevisar la consola.", "Exception", JOptionPane.ERROR_MESSAGE);
            oe.printStackTrace();
        }
    }
    public void escribirLog(String msg)
    {
        try
        {
            System.out.println("[Tract-"+this.getIdentificador()+"]: "+msg);
            LogFileManager log = new LogFileManager(Defaults.TRACTOR_LOG_FILEPATH, true);
            log.writeTo("[Tract-"+this.getIdentificador()+"]: "+msg);
            log.closeWriter();
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "IOException desde escribirLog() de "+this.getClass().getName(), "Exception", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }
}
