package tpsocket.clientes;

import tpsocket.constantes.Defaults;
import tpsocket.logs.LogFileManager;
import tpsocket.tcp.TCPComManager;
import com.google.gson.Gson;
import java.awt.HeadlessException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Date;
import java.util.Random;
import javax.swing.JOptionPane;

/**
 *
 * @author Estercita
 */
public class Clima
{
    // Atributos especificados en el pdf del profe Mancía.
    private String clima,zona, departamento, distrito;
    private Date fecha; 
    private int identificador;
    public Clima(int id)
    {
        
        this.identificador=id;
    }
    public void CargarDatos() throws IOException{
        
        /*BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
        JOptionPane.showMessageDialog(null, "Se solicita datos para el Servidor Clima"
                        , "Dialogo", JOptionPane.INFORMATION_MESSAGE);*/
        this.departamento= Defaults.DataClima[0][0].toString();
        this.distrito = Defaults.DataClima[1][(int) (Math.random() * 4)].toString();
        this.clima=Defaults.DataClima[2][(int)(Math.random()*4)].toString();
        /*System.out.print("Datos del Clima: ");
        this.clima= teclado.readLine();
        System.out.print("Departamento: ");
        this.departamento = teclado.readLine();
        System.out.print("Distrito: ");
        this.distrito = teclado.readLine();
        System.out.print("Zona: ");
        this.distrito = teclado.readLine();*/
        this.zona= Defaults.DataClima[3][(int)(Math.random()*4)].toString();
        this.fecha=new Date();
    }
    public void escribirLog(String msg){
        try
        {
            System.out.println("[Clima-"+this.getID()+"]: "+msg);
            LogFileManager log = new LogFileManager(Defaults.CLIMA_LOGFILEPATH, true);
            log.writeTo("[Clima-"+this.getID()+"]: "+msg);
            log.closeWriter();
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "IOException desde escribirLog() de "+this.getClass().getName(), "Exception", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }  
    public int getID(){
        return this.identificador;
    }
    public String getZona(){
        return this.zona;
    }
    public String getClima(){
        return this.clima;
    }
    public Date getFecha(){
        return this.fecha;
    }
    public String getDepartamento(){
        return this.departamento;
    }
    public String getDistrito(){
        return this.distrito;
    }
    public static void main(String argv[])
    {
        try {
            StringBuilder host;
            System.out.print("IP del Servidor: ");
            BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
            host = new StringBuilder(teclado.readLine());
            
            /** Conexion. */
            Socket clienteSocket;
            clienteSocket = new Socket(InetAddress.getByName(host.toString()), Defaults._SERVER_OF_CLIMA_TCP_PORT);
            
            /** Crear un objeto que maneje los envios. */
            TCPComManager mensajeManager = new TCPComManager();
            Random r=new Random();
//            int id=r.nextInt(100);
            do {
                //int id=r.nextInt(100);
                StringBuilder log;
                /** La entidad Clima es unica y la misma mientras siga enviando msg. */
                Clima c = new Clima(clienteSocket.getLocalPort());
                log = new StringBuilder("Soy la entidad Clima "+c.getID());
                c.escribirLog(log.toString());
                log = log.delete(0, log.length());
                c.CargarDatos();

                /** Crea un objeto JSON. */
                Gson objGson = new Gson();

                /** Gson-near el objeto Satelite. */
                String objetoClimaEnGson = objGson.toJson(c);
                log.append("Objeto Clima formateado a Gson.");
                c.escribirLog(log.toString());
                log = log.delete(0, log.length());

                /** Obtener la longitud del mensaje en bytes. */
                int longitud = objetoClimaEnGson.getBytes().length;

                /** Negociacion - Enviar la longitud al servidor. */
                mensajeManager.sendInt(longitud, clienteSocket);

                /** Si el servidor recibio bien la longitud. */
                if(mensajeManager.readString(clienteSocket).equals("ok"))
                {
                    log.append("Longitud del msg en gson enviada correctamente."
                            + " Enviando al servidor el mensaje gson...");
                    c.escribirLog(log.toString());
                    log = log.delete(0, log.length());
                    /** Envia el mensaje en flujo de bytes. */
                    mensajeManager.sendBytes(objetoClimaEnGson, clienteSocket);
                }

                /** Si el servidor recibio bien el mensaje. */
                if(mensajeManager.readString(clienteSocket).equals("ok"))
                {
                    log.append("Mensaje enviado correctamente.");
                    c.escribirLog(log.toString());
                    JOptionPane.showMessageDialog(null, log, "Mensaje", JOptionPane.DEFAULT_OPTION);
                }
                    
                System.out.println();
            }
            while(JOptionPane.showConfirmDialog(null,
                "Enviar otros Datos de Clima", "Mensaje",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION);
            
            /** Avisa al servidor de su desconexion. */
            mensajeManager.sendInt(0, clienteSocket);
            /** Cierra la conexion cerrando el socket. */
            clienteSocket.close();
        }
        catch(IOException | HeadlessException oe)
        {
            JOptionPane.showMessageDialog(null, "Exception desde main() de Clima."
                    + "\nRevisar la consola.", "Exception", JOptionPane.ERROR_MESSAGE);
            oe.printStackTrace();
        }
    }
}
