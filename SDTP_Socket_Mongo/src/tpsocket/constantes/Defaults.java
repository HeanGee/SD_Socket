package tpsocket.constantes;

/**
 *
 * @author hean
 */
public class Defaults
{
    /** Current OS properties Constants. */
    public static final String CURRENT_PATH = System.getProperty("user.dir");
    public static final char SYS_PATH_SEPARATOR = System.getProperty("file.separator").charAt(0);
    
    /** Constants for LOG files. */
    public static final String SATELITE_LOG_FILEPATH = CURRENT_PATH+";log_Satelite.txt".replace(';', SYS_PATH_SEPARATOR);
    public static final String SERVER_OF_SATELITE_LOGFILEPATH = CURRENT_PATH+";log_ServerOfSatelite.txt".replace(';', SYS_PATH_SEPARATOR);
    public static final String TCPACKAGESENDER_LOGFILEPATH = CURRENT_PATH+";log_TcpackageSender.txt".replace(';', SYS_PATH_SEPARATOR);
    public static final String SERVER_X_LOGFILEPATH = CURRENT_PATH+";log_ServerX.txt".replace(';', SYS_PATH_SEPARATOR);
    public static final String SERVER_TRACTOR_LOGFILEPATH = CURRENT_PATH+";log_ServerTractor.txt".replace(';', SYS_PATH_SEPARATOR);
    public static final String TRACTOR_LOG_FILEPATH = CURRENT_PATH+";log_Tractor.txt".replace(';', SYS_PATH_SEPARATOR);
    public static final String SERVER_OF_CLIMA_LOGFILEPATH = CURRENT_PATH+";log_ServerOfClima.txt".replace(';', SYS_PATH_SEPARATOR);
    public static final String CLIMA_LOGFILEPATH = CURRENT_PATH+";log_Clima.txt".replace(';', SYS_PATH_SEPARATOR);
    
    /** Constants for sockets. */
    public static final String _SERVER_HOST = "localhost";
    public static final String _MONEY_SERVER_HOST = "localhost";
    public static final int _SERVER_OF_SATELITE_TCP_PORT = 6000;
    public static final int _SERVER_OF_TRACTOR_TCP_PORT = 6002;
    public static final int _SERVER_OF_CLIMA_TCP_PORT = 6001;
    public static final int _MONEY_SERVER_UDP_PORT = 6500;
    
    /** Constants for Data Random. */
    public static Object[][] DataSatelite =
    {
        {"Central"},
        {"Fernando de la Mora", "San Lorenzo", "Lambaré", "Capiata"},
        {"10.1", "10.1", "20.2", "30.3"},
        {"-20.2", "-10.1", "30.3", "40.4"}
    };
    public static Object[][] DataClima =
    {
        {"Cordillera"},
        {"Eusebio Ayala", "Caacupe", "Isla Puku", "Caraguatay"},
        {"Nublado, con lluvias dispersas \n Temperatura: 22","Caluroso \n Temperatura: 35","Lluvioso \n Temperatura: 26","Soleado, despejado \n Temperatura: 30"},
        {"Boscosa", "Alta", "Centrica", "Baja"}
        
    };
}
