package tpsocket.tcp;

import tpsocket.constantes.Defaults;
import tpsocket.logs.LogFileManager;
import tpsocket.servidores.ThreadAtenderSatelite;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/** Manejador de comunicacion TCP.
 *
 * @author Hean G
 */
public class TCPComManager
{
    private boolean enviadoCorrectamente;
    public TCPComManager()
    {
    }

    /** Envia un entero al servidor.
     * No retorna mensaje del servidor.
     * @param i es el entero a enviar.
     * @param socket del cliente conectado.
     * @throws IOException si ocurre error con socket.
     */
    public void sendInt(int i, Socket socket) throws IOException
    {
        this.enviadoCorrectamente = false;
        DataOutputStream canalSalida;

        canalSalida = new DataOutputStream(socket.getOutputStream());

        canalSalida.writeInt(i);
        this.enviadoCorrectamente = true;
    }
    
    /** Lee del servidor un mensaje en String.
     * 
     * @param socket del cliente conectado.
     * @return el mensaje del servidor en String.
     * @throws IOException si ocurre error de conexion.
     */
    public String readString(Socket socket) throws IOException
    {
        DataInputStream dis = new DataInputStream(socket.getInputStream());
        return dis.readUTF();
    }
    
    /** Envia mensaje como un String.
     * No retorna respuesta del servidor.
     * 
     * @param mensaje el mensaje en String.
     * @param socket del cliente conectado.
     * @throws IOException si ocurre error de conexion.
     */
    public void sendString(String mensaje,Socket socket) throws IOException
    {
        DataOutputStream env = new DataOutputStream(socket.getOutputStream());
        env.writeUTF(mensaje);
    }
    
    /** Envia mensaje en bytes.
     * No retorna respuesta del servidor.
     * 
     * @param aString el mensaje en String.
     * @param socket del cliente conectado.
     * @throws IOException si ocurre error de conexion.
     */
    public void sendBytes(String aString, Socket socket) throws IOException
    {
        this.enviadoCorrectamente = false;
        DataOutputStream canalSalida;

        canalSalida = new DataOutputStream(socket.getOutputStream());

        /** Enviar al servidor el mensaje en bytes. */
        canalSalida.writeBytes(aString);
        this.enviadoCorrectamente = true;
    }

    public void escribirLog(String msg)
    {
        try {
            LogFileManager log = new LogFileManager(Defaults.TCPACKAGESENDER_LOGFILEPATH, true);
            log.writeTo(msg);
            log.closeWriter();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "IOException desde escribirLog() de "+this.getClass().getName(), "Exception", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(ThreadAtenderSatelite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean isEnviadoCorrectamente() {
        return enviadoCorrectamente;
    }
}