/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpsocket.imgmanager;

import java.io.IOException;

import sun.misc.BASE64Encoder;
import sun.misc.BASE64Decoder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

public class ImgEncoderB64 {

    /**
     * Decode string to image
     * @param imageString The string to decode
     * @return decoded image
     */
    public static BufferedImage decodeToImage(String imageString) {

        BufferedImage image = null;
        byte[] imageByte;
        try {
            BASE64Decoder decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(imageString);  // del string codificado, recupera en bytevector
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte); // crea un streaminput de la img desde el bytevector.
            image = ImageIO.read(bis); // lee en formato imagen desde el streaminput de la imagen
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }

    /**
     * Encode image to string
     * @param image The image to encode
     * @param type jpeg, bmp, ...
     * @return encoded string
     */
    public static String encodeToString(BufferedImage image, String type) {
        String imageString = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, type, bos); // generar en bos, la imagen en formato byteArray
            byte[] imageBytes = bos.toByteArray(); // obtiene en bytes puros del byteArray

            BASE64Encoder encoder = new BASE64Encoder(); // en ecoder instancia on objeto b64
            imageString = encoder.encode(imageBytes); // el objeto encoder codifica la img en b64 desde el vector de bytes.

            bos.close(); // hay que cerrar el flujo de salida de imagen
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageString;
    }

//    public static void main (String args[]) throws IOException {
//        /* Test image to string and string to image start */
//        BufferedImage img = ImageIO.read(new File("unaimg.png"));
//        BufferedImage newImg;
//        String imgstr;
//        imgstr = encodeToString(img, "png");
//        System.out.println(imgstr);
//        newImg = decodeToImage(imgstr);
//        ImageIO.write(newImg, "png", new File("copyofunaimg.png"));
//        /* Test image to string and string to image finish */
//    }
}