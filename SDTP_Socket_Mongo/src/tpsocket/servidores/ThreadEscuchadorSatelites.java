package tpsocket.servidores;

import tpsocket.constantes.Defaults;
import tpsocket.logs.LogFileManager;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Hean G
 */
public class ThreadEscuchadorSatelites extends Thread {
    /** Abstraccion del socket del lado servidor. */
    private final ServerSocket serverSocket;
    
    public ThreadEscuchadorSatelites() throws IOException
    {
        super("ThreadEscuchadorSatelites");
        /** Prepara e inicia el socket servidor para Satelite. */
        this.serverSocket = new ServerSocket(Defaults._SERVER_OF_SATELITE_TCP_PORT);
    }
    
    @Override
    public void run()
    {
        while(true)
        {
            try
            {
                /** Se bloquea, espera una conexion de un Satelite. */
                Socket clientSocket = this.serverSocket.accept();
                
                /**  */
                this.escribirLog("Se conecto un cliente desde el puerto remoto: "+
                        clientSocket.getPort());
                
                /** Comprobar si se cerro la conexion antes de asignarle un hilo. */
                if(!clientSocket.isClosed())
                {
                    /** Generar un nuevo hilo para el cliente conectado. */
                    ThreadAtenderSatelite nuevoSatelite = new ThreadAtenderSatelite(clientSocket);
                    nuevoSatelite.start();
                } else {
                    this.escribirLog("Un cliente satelite cerro su conexion.");
                }
            }
            catch(IOException ex)
            {
                JOptionPane.showMessageDialog(null, "IOException desde run() de "+this.getName(), "Excepcion", JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(ThreadEscuchadorSatelites.class.getName()).log(Level.SEVERE, null, ex);
                this.escribirLog("IOException desde run() de "+this.getName());
            }
        }
    }
    
    void escribirLog(String msg)
    {
        try
        {
            System.out.println(msg);
            LogFileManager log = new LogFileManager(Defaults.SERVER_OF_SATELITE_LOGFILEPATH, true);
            log.writeTo(this.getName()+": "+msg);
            log.closeWriter();
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "IOException desde escribirLog() de "+this.getName(), "Exception", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }
}
