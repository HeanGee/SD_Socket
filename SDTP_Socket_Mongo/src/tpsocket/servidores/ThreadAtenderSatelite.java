package tpsocket.servidores;

import tpsocket.constantes.Defaults;
import tpsocket.imgmanager.ImgEncoderB64;
import tpsocket.logs.LogFileManager;
import tpsocket.clientes.Satelite;
import tpsocket.basededatos.ConexionBD;
import com.google.gson.Gson;
import java.awt.image.BufferedImage;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

public class ThreadAtenderSatelite extends Thread
{
    private final Socket clientSocket;
    private DataOutputStream canalSalida;
    private DataInputStream canalEntrada;
    
    public ThreadAtenderSatelite(Socket clientSocket)
    {
        super("ThreadAtenderSatelite");
        this.clientSocket = clientSocket;
    }

    @Override
    public void run()
    {
        try
        {
            /** Crear canales de entrada y salida. */
            this.canalEntrada = new DataInputStream(this.clientSocket.getInputStream());
            this.canalSalida = new DataOutputStream(this.clientSocket.getOutputStream());
            
            int longitudString;
            StringBuilder log = new StringBuilder();
            do
            {
                log.append("Esperando recibir longitud del mensaje en bytes...");
                this.escribirLog(log.toString());
                log = log.delete(0, log.length());
                /** Negociacion - Obtener la longitud del mensaje en bytes. */
                longitudString = this.canalEntrada.readInt();
                
                if(longitudString>0)
                {
                    log.append("Longitud recibida; ").append(longitudString);
                    this.escribirLog(log.toString());
                    log = log.delete(0, log.length());
                    
                    /** Replay de recepción de longitud. */
                    this.canalSalida.writeUTF("ok");
                    
                    /** Dimensionar un byte[] con el tamanho recibido. */
                    byte[] bytesReceived = new byte[longitudString];

                    /** Cargar bytesReceived, byte a byte, con los bytes que envia el cliente. */
                    log.append("Leyendo bytes del buffer de entrada...");
                    this.escribirLog(log.toString());
                    log = log.delete(0, log.length());
                    
                    for(int i=0; i<longitudString; i++) {
                        bytesReceived[i] = (byte)this.canalEntrada.readByte();
                    }
                    
                    /** Convierte en String el mensaje recibido en bytes. */
                    String contentReceived_inString;
                    contentReceived_inString = new String(bytesReceived);
                    log.append("Lectura del buffer de entrada terminado. Mensaje convertido a String.");
                    this.escribirLog(log.toString());
                    log = log.delete(0, log.length());
                    
                    StringBuilder msgTmp = new StringBuilder(contentReceived_inString);
                    log.append("Mensaje: ").append(msgTmp.substring(0, 130)+"... y mas ...");
                    this.escribirLog(log.toString());
                    log = log.delete(0, log.length());

                    /* --------------------------------------------------------- */
//                    /** GUARDAR EN BASE DE DATOS contentReceived_inString. */
//                    ConexionBD conec = new ConexionBD("localhost" , 27017,"test"); //Crea un Cliente del MongoDB
//                    conec.crear();  //crea la conexion a la BD
//                    System.out.println("***Insertando a la Base de Datos...");
//                    conec.insertar("Satelite",contentReceived_inString);  //Inserta a la Base de Datos
//                    System.out.println("***JSON string insertado***");
                    /* --------------------------------------------------------- */
                    
                    /** Replay de recepción y procesado de msg. */
                    this.canalSalida.writeUTF("ok");

                    /** Registrar log. */
                    log.append("Peticion de Satelite ")
                            .append(clientSocket.getPort())
                            .append(" procesado correctamenete.");
                    this.escribirLog(log.toString());
                    log = log.delete(0, log.length());
                    
                    /* -------------------------------------------------------------- */
                        /** TEST. */
                        /** NO FORMA PARTE DE INDICACIÓN PDF. */
                        /** Crea un objeto JSON. */
                        Gson obJSON = new Gson();

                        /** Des-gson-nea al String, obteniendo un objeto de tipo Satelite. */
                        /** Instancia ese objeto Satelite. */
                        log.append("Construyendo objeto a partir del mensaje...");
                        this.escribirLog(log.toString());
                        log = log.delete(0, log.length());

                        Satelite objSatelite = obJSON.fromJson(contentReceived_inString, Satelite.class);

                        /** lOG. */
                        log.append("Objeto construido correctamente: ").append(objSatelite.getClass());
                        this.escribirLog(log.toString());
                        log = log.delete(0, log.length());


                        /** Probar a recounstruir una imagen. */
                            BufferedImage imagenDelSatelite;
                            imagenDelSatelite = ImgEncoderB64.decodeToImage(objSatelite.getImgProcesadoABase64());
                            String tipo = objSatelite.getTypeOfImg();
                            log.append("Probando reconstruir imagen...");
                            this.escribirLog(log.toString());
                            log = log.delete(0, log.length());
                            ImageIO.write(
                                    imagenDelSatelite, tipo, new File(
                                            "Satelite_"+objSatelite.getIdentificador()
                                            +"_"+objSatelite.getCodCultivo()
                                            +"."+tipo)
                            );
                            log.append("Imagen Reconstruido correctamente.");
                            this.escribirLog(log.toString());
                            log = log.delete(0, log.length());
                    /* -------------------------------------------------------------- */
                    
                }
            }
            while(longitudString>0);    /** mientras el cliente le envie una longitud valida. */
            
            /** Cierre de canales y socket. */
            this.cerrarFlujos(log);
//            if(!this.clientSocket.isClosed() || this.clientSocket.isConnected())
//            {
//                log.append("Cliente cerro conexion."
//                        + " Cerrando flujos en el servidor de Satelite.");
//                this.escribirLog(log.toString());
//                this.canalEntrada.close();
//                this.canalSalida.close();
//                this.clientSocket.close();
//            }
        }
        catch (IOException ex)
        {
            //JOptionPane.showMessageDialog(null, "IOException desde run() de "+this.getName(), "Exception", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(ThreadAtenderSatelite.class.getName()).log(Level.SEVERE, null, ex);
            this.escribirLog("I/O Exception desde run() de "+this.getName()+". SE HA PERDIDO EL MENSAJE.");
            try {
                this.cerrarFlujos(new StringBuilder());
            } catch (IOException ex1) {
                Logger.getLogger(ThreadAtenderSatelite.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } catch (Throwable ex) {
            Logger.getLogger(ThreadAtenderSatelite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void cerrarFlujos(StringBuilder log) throws IOException {
        if(!this.clientSocket.isClosed() || this.clientSocket.isConnected())
        {
            log.append("Cliente cerro conexion."
                    + " Cerrando flujos en el servidor de Satelite.");
            this.escribirLog(log.toString());
            this.canalEntrada.close();
            this.canalSalida.close();
            this.clientSocket.close();
        }
    }
    
    public void escribirLog(String msg)
    {
        try {
            System.out.println("\t[Sat-"+this.clientSocket.getPort()+"]:"+msg);
            LogFileManager log = new LogFileManager(Defaults.SERVER_OF_SATELITE_LOGFILEPATH, true);
            log.writeTo("\t[Sat-"+this.clientSocket.getPort()+"]:"+msg);
            log.closeWriter();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "IOException desde escribirLog() de "+this.getName(), "Exception", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(ThreadAtenderSatelite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
