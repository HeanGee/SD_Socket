package tpsocket.servidores;

import tpsocket.clientes.Clima;
import tpsocket.constantes.Defaults;
import tpsocket.logs.LogFileManager;
import tpsocket.basededatos.ConexionBD;
import com.google.gson.Gson;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
/**
 *
 * @author Estercita
 */
public class ThreadAtenderClima extends Thread
{
    private final Socket clientSocket;
    private DataOutputStream canalSalida;
    private DataInputStream canalEntrada;
    
    public ThreadAtenderClima(Socket clientSocket)
    {
        super("ThreadAtenderClima");
        this.clientSocket = clientSocket;
    }

    @Override
    public void run()
    {
         try
        {
            /** Crear canales de entrada y salida. */
            this.canalEntrada = new DataInputStream(this.clientSocket.getInputStream());
            this.canalSalida = new DataOutputStream(this.clientSocket.getOutputStream());
            
            int longitudString;
            StringBuilder log = new StringBuilder();
            do
            {
                log.append("Esperando recibir longitud del mensaje en bytes...");
                this.escribirLog(log.toString());
                log = log.delete(0, log.length());
                /** Negociacion - Obtener la longitud del mensaje en bytes. */
                longitudString = this.canalEntrada.readInt();
                if(longitudString>0)
                {
                    log.append("Longitud recibida; ").append(longitudString);
                    this.escribirLog(log.toString());
                    log = log.delete(0, log.length());
                    /** Replay de recepción de longitud. */
                    this.canalSalida.writeUTF("ok");
                    
                    /** Dimensionar un byte[] con el tamanho recibido. */
                    byte[] bytesReceived = new byte[longitudString];
                    
                    /** Cargar bytesReceived, byte a byte, con los bytes que envia el cliente. */
                    log.append("Recibiendo mensaje en bytes...");
                    this.escribirLog(log.toString());
                    log = log.delete(0, log.length());
                    for(int i=0; i<longitudString; i++)
                        bytesReceived[i] = (byte)this.canalEntrada.readByte();

                    /** Convierte en String el mensaje recibido en bytes. */
                    String contentReceived_inString;
                    contentReceived_inString = new String(bytesReceived);
                    
                    /** Log. */
                    log.append("Mensaje en bytes recibido. Mensaje convertido a String.");
                    this.escribirLog(log.toString());
                    log = log.delete(0, log.length());
                    
                    /** GUARDAR EN BASE DE DATOS contentReceived_inString. */
//                    ConexionBD conec = new ConexionBD("localhost" , 27017,"test"); //Crea un Cliente del MongoDB
//                    conec.crear();  //crea la conexion a la BD
//                    System.out.println("***Insertando a la Base de Datos...");
//                    conec.insertar("Clima",contentReceived_inString);  //Inserta a la Base de Datos
//                    System.out.println("***JSON string insertado***");

                    /** Replay de recepción y procesado de mensaje. */
                    this.canalSalida.writeUTF("ok");

                    /* -------------------------------------------------------- */
                    /** TEST - NO REQUISITO DEL PDF. */
                    /** Crea un objeto JSON. */
                    Gson obJSON = new Gson();
                    /** Des-gson-nea al String, obteniendo un objeto de tipo Satelite. */
                    /** Instancia ese objeto Satelite. */
                    log.append("Construyendo objeto a partir del mensaje...");
                    this.escribirLog(log.toString());
                    log = log.delete(0, log.length());
                    Clima objClima = obJSON.fromJson(contentReceived_inString, Clima.class);
                    
                    /** */
                    log.append("Objeto construido correctamente: ").append(objClima.getClass());
                    this.escribirLog(log.toString());
                    log = log.delete(0, log.length());

                    /** Registrar log. */
                    log.append("Mensaje de un cliente Clima del puerto: ")
                            .append(clientSocket.getPort())
                            .append(" procesado correctamenete.");
                    this.escribirLog(log.toString());
                    log = log.delete(0, log.length());
                    /* -------------------------------------------------------- */
                    
                }
            }
            while(longitudString>0);    /** mientras el cliente le envie una longitud valida. */
            
            /** Cierre de canales y socket. */
            if(!this.clientSocket.isClosed() || this.clientSocket.isConnected())
            {
                log.append("Cliente cerro conexion."
                        + " Cerrando flujos en el servidor Clima.");
                this.escribirLog(log.toString());
                this.canalEntrada.close();
                this.canalSalida.close();
                this.clientSocket.close();
            }
        }
        catch (IOException ex)
        {
            //JOptionPane.showMessageDialog(null, "IOException desde run() de "+this.getName(), "Exception", JOptionPane.ERROR_MESSAGE);
            System.out.println();
            Logger.getLogger(ThreadAtenderClima.class.getName()).log(Level.SEVERE, null, ex);
            this.escribirLog("I/O Exception desde run() de "+this.getName()+".");
        } catch (Throwable ex) {
            Logger.getLogger(ThreadAtenderClima.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void escribirLog(String msg)
    {
        try {
            System.out.println("\t[Cli-"+this.clientSocket.getPort()+"]:"+msg);
            LogFileManager log = new LogFileManager(Defaults.SERVER_OF_CLIMA_LOGFILEPATH, true);
            log.writeTo("\t[Cli-"+this.clientSocket.getPort()+"]:"+msg);
            log.closeWriter();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "IOException desde escribirLog() de "+this.getName(), "Exception", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(ThreadAtenderClima.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
