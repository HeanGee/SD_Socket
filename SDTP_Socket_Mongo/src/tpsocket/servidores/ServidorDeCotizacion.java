/*
 * Representa al servidor de cotizacion.
 */
package tpsocket.servidores;

import tpsocket.constantes.Defaults;
    import java.net.*;
    import java.io.*;
    import java.text.DateFormat;
    import java.text.SimpleDateFormat;
    import java.util.Date;
import javax.swing.JOptionPane;

public class ServidorDeCotizacion {
   // public static boolean MONEY_SERVER_IS_ON;// = false;
    
    private String cadena;
    private String respuesta = "\n";
    
    private File archivo;
    int puertoServidor;// = Defaults._MONEY_SERVER_UDP_PORT;
    
    @SuppressWarnings("ConvertToTryWithResources")
    public ServidorDeCotizacion(){
        puertoServidor = Defaults._MONEY_SERVER_UDP_PORT;
    }
        
    public void levantarServidor() {
        System.out.println("\n Servidor de COTIZACION.\n");
        try {
            //1) Creamos el socket Servidor de Datagramas (UDP)
            DatagramSocket serverSocket = new DatagramSocket(puertoServidor);

            //2) buffer de datos a enviar y recibir
            byte[] receiveData = new byte[1024];
            byte[] sendData = new byte[1024];


            //3) Servidor siempre esperando
            while (true) {

                receiveData = new byte[1024];

                DatagramPacket receivePacket =
                        new DatagramPacket(receiveData, receiveData.length);


                System.out.println("Esperando a algun cliente... ");
                
                // 4) Receive LLAMADA BLOQUEANTE
                serverSocket.receive(receivePacket);

                System.out.println("Aceptamos un paquete");

                // Datos recibidos e Identificamos quien nos envio
                String datoRecibido = new String(receivePacket.getData());

                InetAddress IPAddress = receivePacket.getAddress();

                int port = receivePacket.getPort();

                System.out.println("De : " + IPAddress + ":" + port);
                System.out.println("Mensaje : " + datoRecibido);
                StringBuilder respuesta2 = new StringBuilder();
                try{
                    //Se lee la cotizacion de un archivo de texto plano llamado cotizacion.txt
                
                    archivo = new File(Defaults.CURRENT_PATH+";cotizacion.txt".replace(';', Defaults.SYS_PATH_SEPARATOR));
                    FileReader FR = new FileReader(archivo);
                    BufferedReader BR = new BufferedReader(FR);
                    
                    while((cadena = BR.readLine())!=null) {
                        //se lee una linea del archivo y se concatena a la respuesta
                        //que se enviara al cliente que realizo una peticion
                        //respuesta = respuesta + cadena + "\n";
                        respuesta2.append(cadena).append("\n");
                    }
                    //se cierra el archivo
                    BR.close();
                    //Se añade la hora y fecha del servidor
                    Date date = new Date();
                    DateFormat dia = new SimpleDateFormat("dd/MM/yyyy");
                    DateFormat hora = new SimpleDateFormat("HH:mm:ss");
                    String fecha ="Actualizado el:  " + dia.format(date) + " a las: "+ hora.format(date);
                    //respuesta = respuesta + fecha;
                    respuesta2.append(fecha);
                
                   
                }
                catch(Exception e){
                    respuesta2 = respuesta2.delete(0, respuesta.length());
                    //respuesta = "Ocurrio un error: " + e.getMessage();
                }
                
                //se envia la respuesta al cliente
                //sendData = respuesta.getBytes();
                sendData = respuesta2.toString().getBytes();
                DatagramPacket sendPacket =
                    new DatagramPacket(sendData, sendData.length, IPAddress,port);
                serverSocket.send(sendPacket);
                respuesta2 = respuesta2.delete(0, respuesta2.length());
            }

        } catch (Exception ex) {
            System.out.println("Puerto UDP " + puertoServidor +" esta ocupado.");
            System.exit(1);
        }
    }
       
    public static void main(String argv[]) {
        ServidorDeCotizacion servidorCot = new ServidorDeCotizacion();
        servidorCot.levantarServidor();
    }
}
