package tpsocket.servidores;

import tpsocket.constantes.Defaults;
import tpsocket.logs.LogFileManager;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Estercita
 */
public class ThreadEscuchadorClima extends Thread {
    /** Abstraccion del socket del lado servidor. */
    private final ServerSocket serverSocket;
    
    public ThreadEscuchadorClima() throws IOException
    {
        super("ThreadEscuchadorClima");
        /** Prepara e inicia el socket servidor para Satelite. */
        this.serverSocket = new ServerSocket(Defaults._SERVER_OF_CLIMA_TCP_PORT);
    }
    
    @Override
    public void run()
    {
        while(true)
        {
            try
            {
                /** Se bloquea, espera una conexion de un Satelite. */
                Socket clientSocket = this.serverSocket.accept();
                
                /**  */
                System.out.println("Se conecto un cliente Clima");
                this.escribirLog("Se conecto un cliente desde el puerto remoto: "+
                        clientSocket.getPort());
                
                /** Comprobar si se cerro la conexion antes de asignarle un hilo. */
                if(!clientSocket.isClosed())
                {
                    /** Generar un nuevo hilo para el cliente conectado. */
                    ThreadAtenderClima nuevoClima = new ThreadAtenderClima(clientSocket);
                    nuevoClima.start();
                    this.escribirLog("Cliente "+clientSocket.getPort()+" asignado al hilo"
                            + " ThreadAtenderClima: "+nuevoClima.getId());
                } else {
                    this.escribirLog("Un cliente Clima cerro su conexion.");
                }
            }
            catch(IOException ex)
            {
                JOptionPane.showMessageDialog(null, "IOException desde run() de "+this.getName(), "Excepcion", JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(ThreadEscuchadorClima.class.getName()).log(Level.SEVERE, null, ex);
                this.escribirLog("IOException desde run() de "+this.getName());
            }
        }
    }
    
    void escribirLog(String msg)
    {
        try
        {
            LogFileManager log = new LogFileManager(Defaults.SERVER_OF_CLIMA_LOGFILEPATH, true);
            log.writeTo(this.getName()+": "+this.getId()+": "+msg);
            log.closeWriter();
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "IOException desde escribirLog() de "+this.getName(), "Exception", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }
}
