
package tpsocket.servidores;

import tpsocket.clientes.Tractor;
import tpsocket.constantes.Defaults;
import tpsocket.logs.LogFileManager;
import tpsocket.basededatos.ConexionBD;
import com.google.gson.Gson;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Fernando
 */
public class ThreadAtenderTractor extends Thread{
    private final Socket clientSocket;
    private DataOutputStream canalSalida;
    private DataInputStream canalEntrada;
    
    public ThreadAtenderTractor(Socket clientSocket)
    {
        super("ThreadAtenderTractor");
        this.clientSocket = clientSocket;
    }

    @Override
    public void run()
    {
        try {
            /** Crear canales de entrada y salida. */
            this.canalEntrada = new DataInputStream(this.clientSocket.getInputStream());
            this.canalSalida = new DataOutputStream(this.clientSocket.getOutputStream());
           
            StringBuilder log = new StringBuilder();
            String mensajeGson;
            do {
                /**Lee el mensaje desde el cliente**/
                mensajeGson=this.canalEntrada.readUTF();

                /** GUARDAR EN BASE DE DATOS mensajeGson. */
//                ConexionBD conec = new ConexionBD("localhost" , 27017,"test"); //Crea un Cliente del MongoDB
//                conec.crear();  //crea la conexion a la BD
//                System.out.println("***Insertando a la Base de Datos...");
//                conec.insertar("Tractor",mensajeGson);  //Inserta a la Base de Datos
//                System.out.println("***JSON string insertado***");
                
                /** Replay sobre recepcion y procesado de msg. */
                this.canalSalida.writeUTF("ok");

                /** Logs. */
                log.append("Cliente tractor: ").append(mensajeGson);
                this.escribirLog(log.toString());
                log = log.delete(0, log.length());

                /** Si la peticion no es desconexion. */
                if(!mensajeGson.equals("no")) {
                    /** Crea un objeto JSON. */
                    Gson obJSON = new Gson();
                    /** Des-gson-nea al String, obteniendo un objeto de tipo Satelite. */
                    /** Instancia ese objeto Satelite. */
                    log.append("Construyendo objeto a partir del mensaje...");
                    this.escribirLog(log.toString());
                    log = log.delete(0, log.length());
                    Tractor objTractor = obJSON.fromJson(mensajeGson, Tractor.class);
                    log.append("Objeto construido correctamente: ").append(objTractor.getClass());
                    log.append("Mensaje recibido correctamente del Tractor: ").append(objTractor.getIdentificador());
                    this.escribirLog(log.toString());
                    log = log.delete(0, log.length());
                }
            } while(!mensajeGson.equals("no"));
            /****/
            if(!this.clientSocket.isClosed() || this.clientSocket.isConnected())
            {
                log.append("Cliente cerro conexion."
                        + " Cerrando flujos en el servidor de Tractor.");
                this.escribirLog(log.toString());
                this.canalEntrada.close();
                this.canalSalida.close();
                this.clientSocket.close();
            }
        } catch(IOException ex){
            System.out.println();
            Logger.getLogger(ThreadAtenderSatelite.class.getName()).log(Level.SEVERE, null, ex);
            this.escribirLog("I/O Exception desde run() de "+this.getName()+".");
        } catch (Throwable ex) {
            Logger.getLogger(ThreadAtenderTractor.class.getName()).log(Level.SEVERE, null, ex);
        }      
    }
    
    public void escribirLog(String msg)
    {
        try {
            System.out.println("\t[Trac-"+this.clientSocket.getPort()+"]:"+msg);
            LogFileManager log = new LogFileManager(Defaults.SERVER_TRACTOR_LOGFILEPATH, true);
            log.writeTo("\t[Trac-"+this.clientSocket.getPort()+"]:"+msg);
            log.closeWriter();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "IOException desde escribirLog() de "+this.getName(), "Exception", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(ThreadAtenderTractor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
