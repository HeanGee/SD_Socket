/*
 * Hace las consultas al servidor de cotizacion.
 */
package tpsocket.servidores;

import tpsocket.constantes.Defaults;
import java.awt.HeadlessException;
    import java.net.*;
    import java.io.*;
    import javax.swing.*;

public class cons_cot {
    String direccionServidor;// = Defaults._MONEY_SERVER_HOST;
    int puertoServidor;// = Defaults._MONEY_SERVER_UDP_PORT;
    
    public cons_cot()
    {
        // Datos necesario
        //String direccionServidor = "127.0.0.1";
        direccionServidor = Defaults._MONEY_SERVER_HOST;
        puertoServidor = Defaults._MONEY_SERVER_UDP_PORT;
    }
    
    public void consultarCot() {
        try {
//
//            BufferedReader inFromUser;
//            inFromUser = new BufferedReader(new InputStreamReader(System.in));
//
            DatagramSocket clientSocket = new DatagramSocket();

            InetAddress IPAddress = InetAddress.getByName(direccionServidor);
            System.out.println("Intentando conectar a = " + IPAddress + ":" + puertoServidor +  " via UDP...");

            byte[] sendData = new byte[1024];
            byte[] receiveData = new byte[1024];

            String sentence = "¿Cual es la cotizacion del dia?";
            sendData = sentence.getBytes();

            System.out.println("Enviar " + sendData.length + " bytes al servidor.");
            DatagramPacket sendPacket =
                    new DatagramPacket(sendData, sendData.length, IPAddress, puertoServidor);
            
            int seguirIntentando = JOptionPane.YES_OPTION;
            while(seguirIntentando == JOptionPane.YES_OPTION)
            {
                clientSocket.send(sendPacket);

                DatagramPacket receivePacket =
                        new DatagramPacket(receiveData, receiveData.length);

                System.out.println("Esperamos si viene el paquete");

                //Vamos a hacer una llamada BLOQUEANTE entonces establecemos un timeout maximo de espera
                //clientSocket.setSoTimeout(10000);
                int timeout = 5000;
                clientSocket.setSoTimeout(timeout);
                try {
                    // ESPERAMOS LA RESPUESTA, BLOQUENTE
                    clientSocket.receive(receivePacket);

                    seguirIntentando = JOptionPane.NO_OPTION;

                    String modifiedSentence = new String(receivePacket.getData()).trim();
                    InetAddress returnIPAddress = receivePacket.getAddress();
                    int port = receivePacket.getPort();

                    JOptionPane.showMessageDialog(null,
                            "Respuesta desde =  " + returnIPAddress + ":" + port +
                                    "\n" + modifiedSentence, "COTIZACION DEL DIA", JOptionPane.INFORMATION_MESSAGE);

                } catch (SocketTimeoutException ste) {
                    seguirIntentando = JOptionPane.showConfirmDialog(
                            null, "Servidor de Coti no responde.\n" + "Intentar nuevamente?", "Warning", JOptionPane.YES_NO_OPTION);
                }
            }
            System.out.println("Cerrando socket de consultadorCoti");
            clientSocket.close();
            System.out.println("Socket de ConsultadorCoti cerrado correctamente.");
        } catch (UnknownHostException ex) {
            System.err.println(ex);
        } catch (IOException | HeadlessException ex) {
            System.err.println(ex);
        }
    }
}

