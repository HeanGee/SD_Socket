package tpsocket.servidores;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;

/** Representa a nuestro servidor de la especificacion.
 *
 * @author hean
 */
public class Servidor {
    /** Escuchadores TCP. */
    private final ThreadEscuchadorSatelites escuchadorSatelite;
    private final ThreadEscuchadorTractor escuchadorTractor;
    private final ThreadEscuchadorClima escuchadorClima;
    
    /** Peticiones UDP. */
    private final ThreadConsultadorCotizacion consultaMonedaDia;
    
    public Servidor() throws IOException
    {
        this.escuchadorSatelite = new ThreadEscuchadorSatelites();
        this.escuchadorTractor = new ThreadEscuchadorTractor();
        this.escuchadorClima = new ThreadEscuchadorClima();
        
        this.consultaMonedaDia = new ThreadConsultadorCotizacion();
//        this.pantalla = new ThreadScreen(this.escuchadorTractor);
    }
    
    public void startServidor()
    {
        System.out.println("\n Servidor de Satelite, Clima, Tractor.\n");
        /** Inicia escuchador para cada tipo de Entidades. */
        this.escuchadorSatelite.start();
        this.escuchadorTractor.start();
        this.escuchadorClima.start();
        
        /** Hilo independiente para hacer consultas de cotizacion. */
        this.consultaMonedaDia.start();
        
        /** Miscelanea. */
//        this.pantalla.start();
    }
    
    public static void main(String argv[])
    {
        try {
            Servidor servidor = new Servidor();
            servidor.startServidor();
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
