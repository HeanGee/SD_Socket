/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpsocket.logs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;

/** Maneja metodos para generar historiales en un archivo.
 *
 * @author hean
 */
public class LogFileManager {
    private File fileOpened = null;
    private FileWriter fileWriter = null;
    
    /** Constructor que recibe el nombre de un archivo.
     * 
     * @param path      String que representa el nombre del archivo. Puede ser representado
     *                  como un nombre o la ruta completa.
     * @param sobreEscribir indica si se sobreescribe o no con true o false respectivamente.
     * @throws IOException si el archivo no se encuentra o no pudo crearlo.
     */
    public LogFileManager(String path, boolean sobreEscribir) throws IOException {
        this.fileOpened = new File(path);
        this.fileWriter = new FileWriter(path, sobreEscribir);
    }
    
    /** Imprime en la consola las lineas leidas de un archivo.
     * 
     * @throws FileNotFoundException si el archivo no fue creado, o no puede ser accedido.
     * @throws IOException si ocurre un error al acceder al archivo.
     */
    public void readFrom() throws FileNotFoundException, IOException
    {
        if(this.fileOpened != null)
        {
            FileReader fr = new FileReader(fileOpened);
            BufferedReader br = new BufferedReader(fr);
            StringBuilder mensaje = new StringBuilder();
            while( !((mensaje.append(br.readLine())).toString().equals("null")) )
            {
                System.out.println(mensaje);
                mensaje.delete(0, mensaje.length());
            }
        }
    }
    
    /** Escribe a un archivo especificado en su instanciacion con el String logString.
     * 
     * @param logMesage String que contiene la linea de texto a guardar en el archivo.
     */
    public void writeTo(String logMesage)
    {
        if(this.fileWriter != null)
        {
            PrintWriter pr = new PrintWriter(fileWriter);
            pr.println(new GregorianCalendar().getTime().toString()+" - "+logMesage);
        } 
        else
        {
            JOptionPane.showMessageDialog(null,
                    "Error escribiendo en archivo.\n"
                            + "No hay archivo al cual escribir.", "ERROR",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /** Cierra el escritor de archivo interno.
     * 
     * @throws IOException si ocurre un error al acceder al archivo.
     */
    public void closeWriter() throws IOException {
        this.fileWriter.close();
    }
    
//    public static void main(String argv[]) {
//        try {
//            LogFileManager fileManager = new LogFileManager("daniel.txt");
//            BufferedReader inFromUser =
//                    new BufferedReader(new InputStreamReader(System.in));
//            String texto;
//            while( !((texto = inFromUser.readLine()).toString().equals("null")) ) {
//                fileManager.writeTo(texto);
//            }
//            fileManager.closeWriter();
//            fileManager.readFrom();
//        } catch (IOException ex) {
//            Logger.getLogger(LogFileManager.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
}